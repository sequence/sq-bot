# frozen_string_literal: true

module MergeRequestHelper
  def has_open_merge_requests?
    mr_url = resource['_links'][:self] + '/related_merge_requests'
    mr_count = network.query_api_cached(mr_url).count {|m| m[:state].eql? 'opened' }
    mr_count > 0
  end
  def has_failed_merge_requests?
    mr_url = resource['_links'][:self] + '/related_merge_requests'
    mr_count = network.query_api_cached(mr_url).count {|m| m[:pipeline].nil? || m[:pipeline][:status].eql?('failed') }
    mr_count > 0
  end
  def get_current_iteration
    iter_url = 'https://gitlab.com/api/v4/groups/4000070/iterations?state=current'
    current_iter = network.query_api_cached(iter_url)
    current_iter[0][:id]
  end
end

Gitlab::Triage::Resource::Context.include MergeRequestHelper
