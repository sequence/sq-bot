resource_rules:
  issues:
    rules:
      - name: Set wf::backlog label on new issues
        conditions:
          state: opened
          forbidden_labels:
            - wf::backlog
            - wf::planning
            - wf::next
            - wf::doing
            - wf::waiting
            - wf::review
            - wf::release
            - wf::abandoned
            - wf::success
            - dependencies
        actions:
          comment: |
            {{author}} Opened issue was missing a workflow label.
            Automatically adding ~"wf::backlog"
          labels:
            - wf::backlog

      - name: Set wf::doing label on issues with MR
        conditions:
          state: opened
          labels:
            - wf::{backlog,planning,next}
          forbidden_labels:
            - dependencies
          ruby: has_open_merge_requests?
        actions:
          comment: |
            This issue appears to be in progress (assigned and has an associated MR).
            Automatically adding ~"wf::doing"
          labels:
            - wf::doing

      - name: Issues must have a type label
        conditions:
          state: opened
          forbidden_labels:
            - backstage
            - bug::possible
            - bug::confirmed
            - bug::regression
            - bug::upstream
            - documentation
            - feature
            - support
            - testing::integration
            - testing::uat
            - testing::unit
            - dependencies
            - missing type
        actions:
          comment: |
            {{author}} This issue is missing a type label:
              - ~backstage
              - ~bug::possible
              - ~bug::confirmed
              - ~bug::regression
              - ~bug::upstream
              - ~documentation
              - ~feature
              - ~support
              - ~testing::integration
              - ~testing::uat
              - ~testing::unit
          labels:
            - missing type

      - name: Bugs must have a severity
        conditions:
          state: opened
          labels:
            - bug::{possible,confirmed,regression,upstream}
          forbidden_labels:
            - severity::1
            - severity::2
            - severity::3
            - severity::4
            - missing severity
        actions:
          comment: |
            {{author}} This bug is missing a `severity` label.
          labels:
            - missing severity

      - name: Bugs must have a priority
        conditions:
          state: opened
          labels:
            - bug::{possible,confirmed,regression,upstream}
          forbidden_labels:
            - priority::1
            - priority::2
            - priority::3
            - priority::4
            - missing priority
        actions:
          comment: |
            {{author}} This bug is missing a `priority` label.
          labels:
            - missing priority

      - name: Support issues must have a priority
        conditions:
          state: opened
          labels:
            - support
          forbidden_labels:
            - priority::1
            - priority::2
            - priority::3
            - priority::4
            - missing priority
        actions:
          comment: |
            {{author}} This support issue is missing a `priority` label.
          labels:
            - missing priority

      # - name: Features that are in progress need a priority
      #   conditions:
      #     state: opened
      #     labels:
      #       - '{feature,backstage,documentation,testing::integration,testing::uat,testing::unit}'
      #       - wf::doing
      #     forbidden_labels:
      #       - priority::1
      #       - priority::2
      #       - priority::3
      #       - priority::4
      #       - excludecl
      #       - dependencies
      #       - missing priority
      #     ruby: has_open_merge_requests?
      #   actions:
      #     comment: |
      #       {{assignee}} This feature is missing a `priority` label.
      #     labels:
      #       - missing priority

      # - name: Issues in progress must have a weight
      #   conditions:
      #     state: opened
      #     labels:
      #       - wf::doing
      #     weight: None
      #     forbidden_labels:
      #       - excludecl
      #       - dependencies
      #       - missing weight
      #   actions:
      #     comment: |
      #       {{assignee}} This issue is missing a weight.
      #     labels:
      #       - missing weight

      - name: 'Bugs, testing and features need an area:: label'
        conditions:
          state: opened
          labels:
            - '{feature,bug::possible,bug::confirmed,bug::regression,bug::upstream,testing::integration,testing::uat,testing::unit}'
          forbidden_labels:
            - area::audit
            - area::cicd
            - area::connectors
            - area::coresdk
            - area::db/storage
            - area::dif
            - area::industry
            - area::infrastructure
            - area::logging
            - area::monitoring
            - area::orchestrator
            - area::performance
            - area::release/packaging
            - area::runner
            - area::scl
            - area::security
            - area::steps
            - area::tooling
            - area::ui
            - dependencies
            - missing area
            - wf::abandoned
        actions:
          comment: |
            {{author}} This issue is missing an `area` label.
          labels:
            - missing area

      - name: area::testing has been deprecated
        conditions:
          state: opened
          labels:
            - area::testing
        actions:
          comment: |
            {{author}} The label ~"area::testing" has been deprecated.
            Please set the issue type to a `testing::` label and use an `area::`
            label to specify what is being tested.
          remove_labels:
            - area::testing

      - name: Issues in progress or done must have an assignee
        conditions:
          labels:
            - wf::{planning,doing,review,release,waiting,success}
          forbidden_labels:
            - dependencies
            - missing assignee
          ruby: resource['assignees'].count == 0
        actions:
          comment: |
            This issue is missing an assignee.
          labels:
            - missing assignee

      - name: Issues need to have a description
        conditions:
          state: opened
          labels:
            - '{feature,documentation,bug::possible,bug::confirmed,bug::regression,bug::upstream,testing::integration,testing::uat,testing::unit}'
          forbidden_labels:
            - wf::backlog
            - excludecl
            - dependencies
            - missing description
          ruby: resource['description'].to_s.length <= 10
        actions:
          comment: |
            {{author}} This issue is missing a description.
          labels:
            - missing description

      - name: keyfeature issues must have a changelog summary
        conditions:
          state: closed
          labels:
            - keyfeature
          forbidden_labels:
            - missing summary
          ruby: resource['description'] !~ /#+ Changelog.*?\r?\n(.*)$/
        actions:
          comment: |
            {{assignee}} This issue is missing a 'changelog summary' in the description.
          labels:
            - missing summary

      - name: Add keyfeature label to issues with a changelog summary
        conditions:
          forbidden_labels:
            - missing summary
            - keyfeature
          ruby: resource['description'] =~ /#+ Changelog.*?\r?\n(.*)$/
        actions:
          comment: |
            {{assignee}} This issue has a 'Changelog Summary' section in the description
            but is missing a ~keyfeature label.
          labels:
            - keyfeature

      # - name: stale issues
      #   conditions:
      #     state: opened
      #     date:
      #       attribute: created_at
      #       condition: older_than
      #       interval_type: months
      #       interval: 6
      #     forbidden_labels:
      #       - stale
      #   actions:
      #     comment: |
      #       {{author}} It's been a while...
      #     labels:
      #       - stale

  merge_requests:
    rules:
      - name: Remove workflow labels from Merge Requests
        conditions:
          labels:
            - wf::{backlog,planning,next,doing,waiting,review,release,abandoned,success}
        actions:
          remove_labels:
            - wf::backlog
            - wf::planning
            - wf::next
            - wf::doing
            - wf::waiting
            - wf::review
            - wf::release
            - wf::abandoned
            - wf::success

      - name: Merge Requests in progress must have an assignee
        conditions:
          state: opened
          forbidden_labels:
            - excludecl
            - dependencies
            - missing assignee
          ruby: resource['assignees'].count == 0
        actions:
          comment: |
            This merge request is missing an assignee.
          labels:
            - missing assignee
