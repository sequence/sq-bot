# Automatic Issue and MR labels

This project uses [gitlab-triage](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage)
to automatically add/remove labels from issues and merge requests based
on a set of rules.

It runs across the following groups:

- [sequence/connectors](https://gitlab.com/sequence/connectors)
- [sequence/containers](https://gitlab.com/sequence/containers)
- [sequence/templates](https://gitlab.com/sequence/templates)
- [sequence/utilities](https://gitlab.com/sequence/utilities)

And projects:

- [sequence/connectormanager](https://gitlab.com/sequence/connectormanager)
- [sequence/console](https://gitlab.com/sequence/console)
- [sequence/core](https://gitlab.com/sequence/core)
- [sequence/scl-editor](https://gitlab.com/sequence/scl-editor)
- [sequence/scl-vscode](https://gitlab.com/sequence/scl-vscode)
- [sequence/sequence-docs](https://gitlab.com/sequence/sequence-docs)

## Rules

### Run every 15 minutes (08:00 - 23:00 GMT)

- Set `wf::backlog` label on new issues
- Set `wf::doing` label on issues with MR
- Issues must have a type label
- Bugs must have a severity
- Bugs must have a priority
- Support issues must have a priority
- Features that are in progress need a priority (disabled)
- Issues in progress must have a weight (disabled)
- Bugs and features need an `area::*` label
- `area::testing` has been deprecated
- Issues in progress or done must have an assignee
- Issues need to have a description
- `keyfeature` issues must have a changelog summary
- Add `keyfeature` label to issues with a changelog summary
- Remove workflow labels from Merge Requests
- Merge Requests in progress must have an assignee

### Run every 4ish hours (07:00 - 22:00 GMT)

- Set `wf::success` label on closed issues
- Closed bugs should not have a `bug::possible` label
- Remove `missing type` label if issue has type
- Remove `missing severity` label if bug has a severity
- Remove `missing priority` label if bug has a priority
- Remove `missing priority` label if support issue has a priority
- Remove `missing priority` label if feature issue has a priority
- Remove `missing priority` label if feature issue is closed
- Remove `missing weight` label if issue has a weight
- Remove `missing area` label if issue has an area
- Remove `missing testing` type label if issue has a testing::\* label
- Remove `missing assignee` label if issue has an assignee
- Remove `missing description` label if issue has a description
- Remove `missing milestone` label if issue has a milestone
- Remove `missing milestone` label if issue is closed
- Remove `missing summary` label if issue has a changelog summary
- Remove `missing assignee` label if merge request has an assignee

### Run once a day at 7am (currently disabled)

- Assign failed update dependencies to current iteration

### Run every Thursday (currently disabled)

- Creates a summary of all issues with a `missing *` label.

## Testing

```
gitlab-triage --dry-run --token $GITLAB_API_TOKEN --source-id sequence --source groups > output.log
```

Run the `parse-output.ps1` to convert the output into a csv.
