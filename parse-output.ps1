
$rule = ''
$issue = ''

$all = Get-Content .\output.log | % {

    if ($_ -match 'Gathering resources for rule: (.+)') {
        $rule = $Matches[1]
        Write-Verbose "Found rule $rule"
    }

    if ($_ -match '^\.?# (.+)') {
        $issue = $Matches[1]
        Write-Verbose "Found issue/MR issue"
        [PSCustomObject]@{
            Rule  = $rule
            Issue = $issue
        }
    }

}

$all.Count

$all | Export-Csv .\output.csv
