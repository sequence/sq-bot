resource_rules:
  issues:
    rules:
      - name: Set wf::success label on closed issues
        conditions:
          state: closed
          labels:
            - wf::{backlog,next,doing,review,release,waiting,planning}
        actions:
          comment: |
            This issue has been closed but the workflow status hasn't been updated.
            Automatically adding ~"wf::success"
          labels:
            - wf::success

      - name: Closed bugs should not have a bug::possible label
        conditions:
          state: closed
          labels:
            - bug::possible
            - wf::success
        actions:
          comment: |
            {{assignee}} Closed bugs should not have a ~"bug::possible" label.
            Changing to ~"bug::confirmed" since this issue is closed and ~"wf::success".
          labels:
            - bug::confirmed

      - name: Remove missing type label if issue has type
        conditions:
          labels:
            - '{backstage,bug::possible,bug::confirmed,bug::regression,bug::upstream,documentation,feature,support,testing::integration,testing::uat,testing::unit}'
            - missing type
        actions:
          remove_labels:
            - missing type

      - name: Remove missing severity label if bug has a severity
        conditions:
          labels:
            - bug::{possible,confirmed,regression,upstream}
            - '{severity::1,severity::2,severity::3,severity::4}'
            - missing severity
        actions:
          remove_labels:
            - missing severity

      - name: Remove missing priority label if bug has a priority
        conditions:
          labels:
            - bug::{possible,confirmed,regression,upstream}
            - '{priority::1,priority::2,priority::3,priority::4}'
            - missing priority
        actions:
          remove_labels:
            - missing priority

      - name: Remove missing priority label if support issue has a priority
        conditions:
          labels:
            - support
            - '{priority::1,priority::2,priority::3,priority::4}'
            - missing priority
        actions:
          remove_labels:
            - missing priority

      - name: Remove missing priority label if feature issue has a priority
        conditions:
          labels:
            - '{feature,backstage,documentation}'
            - '{priority::1,priority::2,priority::3,priority::4}'
            - missing priority
        actions:
          remove_labels:
            - missing priority

      - name: Remove missing priority label if feature issue is closed
        conditions:
          state: closed
          labels:
            - '{feature,backstage,documentation}'
            - missing priority
        actions:
          remove_labels:
            - missing priority

      - name: Remove missing weight label if issue has a weight
        conditions:
          weight: Any
          labels:
            - missing weight
        actions:
          remove_labels:
            - missing weight

      - name: Remove missing area label if issue has an area
        conditions:
          labels:
            - area::{audit,cicd,connectors,coresdk,db/storage,dif,industry,infrastructure,logging,monitoring,orchestrator,performance,release/packaging,runner,scl,security,steps,tooling,ui}
            - missing area
        actions:
          remove_labels:
            - missing area

      - name: Remove missing assignee label if issue has an assignee
        conditions:
          labels:
            - missing assignee
          ruby: resource['assignees'].count > 0
        actions:
          remove_labels:
            - missing assignee

      - name: Remove missing description label if issue has a description
        conditions:
          labels:
            - missing description
          ruby: resource['description'].to_s.length > 10
        actions:
          remove_labels:
            - missing description

      - name: Remove missing milestone label if issue has a milestone
        conditions:
          labels:
            - missing milestone
          ruby: resource['milestone']
        actions:
          remove_labels:
            - missing milestone

      - name: Remove missing milestone label if issue is closed
        conditions:
          state: closed
          labels:
            - missing milestone
        actions:
          remove_labels:
            - missing milestone

      - name: Remove missing summary label if issue has a changelog summary
        conditions:
          labels:
            - keyfeature
            - missing summary
          ruby: resource['description'] =~ /#+ Changelog.*?\r?\n(.*)$/
        actions:
          remove_labels:
            - missing summary

      - name: Remove missing summary label if keyfeature label has been removed
        conditions:
          labels:
            - missing summary
          forbidden_labels:
            - keyfeature
        actions:
          remove_labels:
            - missing summary

  merge_requests:
    rules:
      - name: Remove missing assignee label if merge request has an assignee
        conditions:
          labels:
            - missing assignee
          ruby: resource['assignees'].count > 0
        actions:
          remove_labels:
            - missing assignee
